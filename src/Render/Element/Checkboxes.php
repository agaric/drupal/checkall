<?php

namespace Drupal\checkall\Render\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\Checkboxes as OriginalCheckboxes;

/**
 * Adds checkboxes support for #checkall property.
 */
class Checkboxes extends OriginalCheckboxes {

  /**
   * {@inheritdoc}
   */
  public static function preRenderCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);

    if (!isset($element['#checkall'])) {
      return $element;
    }

    $weight = ($element['#checkall'] === 'before') ? -2000 : 2000;

    $element['checkall'] = [
      '#type' => 'checkbox',
      '#title' => t('Check all'),
      '#attributes' => [
        'class' => [
          Html::cleanCssIdentifier('checkall'),
        ],
      ],
      '#attached' => [
        'library' => [
          'checkall/checkall',
        ],
      ],
      '#weight' => $weight,
      '#id' => Html::getUniqueId('checkall'),
    ];

    return $element;
  }

}
