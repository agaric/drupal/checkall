/**
 * @file
 * Bootstraps the checkall script for eligible checkboxes.
 */

(function ($, Drupal) {
  /* global Drupal */
  Drupal.behaviors.checkall = {
    attach: function (context, settings) {
      $('.checkall', context).once('checkall-processed').change(function () {
        var $this = $(this);
        $checks = $('input', $this.parents('div').first().siblings());
        if ($this.is(':checked')) {
          $checks.prop('checked', true);
        }
        else {
          $checks.prop('checked', false);
        }
      });
    }
  };
/* global jQuery */
})(jQuery, Drupal);
